/**
 * 
 */
package cn.wwy.service;

import cn.wwy.domain.ResultInfo;

/**
 * @author wangwy
 *
 */
public interface RPCInterface {

    /**
     * 通过dubbo实现远程RPC调用接口信息
     */
    public ResultInfo getRPCInfo();
}
