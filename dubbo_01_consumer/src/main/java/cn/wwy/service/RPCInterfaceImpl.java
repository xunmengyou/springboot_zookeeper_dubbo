/**
 * 
 */
package cn.wwy.service;

import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Reference;

import cn.wwy.domain.ResultInfo;

/**
 * @author wangwy
 *  这里是专门实现，从dubbo服务上获得需要的内容
 */

@Component
public class RPCInterfaceImpl implements RPCInterface{
	
	@Reference(version = "1.0.0")
    RPCInterface rpcInterface;//调用远程接口的实现类
	@Override
	public ResultInfo getRPCInfo() {
		// TODO Auto-generated method stub
		ResultInfo resultInfo = rpcInterface.getRPCInfo();
        return resultInfo;
	}
}
