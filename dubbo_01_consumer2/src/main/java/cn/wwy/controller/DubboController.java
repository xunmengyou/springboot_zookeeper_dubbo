/**
 * 
 */
package cn.wwy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.wwy.domain.ResultInfo;
import cn.wwy.service.RPCInterfaceImpl;

/**
 * @author wangwy
 *
 */

@Controller
public class DubboController {
	@Autowired
	private RPCInterfaceImpl rpcInterface;

	@RequestMapping("/dubbo")
	@ResponseBody
	public String errorNginxTest() {
		ResultInfo resultInfo = rpcInterface.getRPCInfo();
		return "result:" + resultInfo.getResultInfo();
	}

}
