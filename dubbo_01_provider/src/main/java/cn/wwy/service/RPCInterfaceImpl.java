/**
 * 
 */
package cn.wwy.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;

import cn.wwy.domain.ResultInfo;

/**
 * @author wangwy
 *  这里是专门实现，从dubbo服务上获得需要的内容
 */

/**
 * @author wangwy
 *  RPC接口的实现类
 */
//注册为 Dubbo 服务
@Service(version ="1.0.0")
public class RPCInterfaceImpl implements RPCInterface{
	@Override
	public ResultInfo getRPCInfo() {
		// TODO Auto-generated method stub
		ResultInfo resultInfo = new ResultInfo("我是RPC返回值1");
		return resultInfo;
	}
}
